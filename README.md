# Migrating from earlier version? (there are no versions yet) please read **install** and **usage** thouroughly.
## Python script for collecting SentinelOne customers data from the management portal.

## Overview

    Python script to call the SentinelOne API to return some data and write it to a CSV file.

## Prerequisites

- Python (minimum required version: ?)
- tested with python 3.11.2

## Installation

    1. Clone the repository:

       git clone https://gitlab.com/TheNetworkDoctor/sentinelone-api-sites-customers-csv-generator.git

    2. Navigate to the project directory:

        cd your-repo

    3. Set up a virtual environment (optional but recommended):

        python -m venv myenv
        source myenv/bin/activate

    4. Install the required dependencies:

        pip install -r requirements.txt


## Configuration

There is no configuration any more, all variables are changed to command line args, see 'Usage'.

## Usage 

    python3 app.py --help

    usage: app.py [-h] --url URL --token TOKEN [--url-path [URL_PATH]] [--extra [EXTRA]]

    SentinelOne API script to gather customer (quantity) data from the cloud portal

    options:
      -h, --help            show this help message and exit
      --url URL             URL argument (default: https://sentinelone.com)
      --token TOKEN         Token argument (default: mytoken)
      --url-path [URL_PATH]
                            URL path argument (default: web/api/v2.1/sites)
      --extra [EXTRA]       Extra argument (default: ?limit=)

## Output

Generates a .csv file in the project directory, no text is displayed in the python script.

## Example
    python3 app.py --url https://thisurlisnottherealone.com --token thistokendoesnotexist
## Example output csv
(After conversion to .md for display purposes).

id | name | sku | activelicenses | updatedat
---|---|---|---|---
0001 | cust-01 | full | 300 | 2022-09-10
0002 | cust-02 | half-full | 20 | 2022-09-10
0003 | cust-03 | full | 123 | 2022-09-10
0004 | cust-04 | active | 42 | 2022-09-10
0005 | cust-05 | full | 69 | 2022-09-10

## Troubleshooting

re-run the install procedure, still issue, create a issue on gitlab.

## Contributing

Please contribute if you want.

## License
Released under the WTFPL.

<a href="http://www.wtfpl.net/"><img src="http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png" width="80" height="15" alt="WTFPL" /></a>
