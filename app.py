import requests
import csv
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='SentinelOne API script to gather customer (quantity) data from the cloud portal')
    
    # Add command-line arguments
    parser.add_argument('--url', default='https://sentinelone.com', help='URL argument (default: https://sentinelone.com)', required=True)
    parser.add_argument('--token', default='mytoken', help='Token argument (default: mytoken)', required=True)
    parser.add_argument('--url-path', default='web/api/v2.1/sites', nargs='?', help='URL path argument (default: web/api/v2.1/sites)')
    parser.add_argument('--extra', default='?limit=', nargs='?', help='Extra argument (default: ?limit=)')

    args = parser.parse_args()

# Map the arguments to variables
url = args.url
url_path = args.url_path
token = args.token
extra = args.extra

def join_url(*args, separator='/'):
    """
    Joins the given URL components together using the specified separator.

    Args:
        *args: Variable number of URL components to be joined.
        separator (str): Separator character to be used. Defaults to '/'.

    Returns:
        str: Joined URL string.

    """
    if separator == '/':
        return "/".join(map(lambda x: str(x).rstrip('/'), args))
    else:
        return "".join(map(lambda x: str(x).rstrip('/'), args))


def s1get_combined(url=args.url, url_path=args.url_path, extra=args.extra, token=args.token, payload="", content_type=""):
    """
    Combined function to perform API requests with optional URL construction.

    Args:
        url (str): Base URL.
        url_path (str): Path to append to the base URL.
        extra (str): Additional path or query parameters to append to the URL (optional).
        token (str): Authorization token.
        payload (str): Payload data (optional).
        content_type (str): Content type of the request (optional).

    Returns:
        dict: Response JSON object.

    """
    # Construct the initial call URL using the base URL and path
    call_url = join_url(url, url_path)
    call_payload = payload
    headers = {
        "Content-Type": content_type,
        "Authorization": 'Apitoken ' + token
    }
    # Make the initial request
    response = requests.request("GET", call_url, data=call_payload, headers=headers)
    response_json = response.json()

    if extra is not None:
        # If extra parameter is provided, construct the URL with additional components
        total_items = response_json['pagination']['totalItems']
        call_url = join_url(call_url, extra, total_items, separator="")

    # Make the final request with the updated URL
    response = requests.request("GET", call_url, data=call_payload, headers=headers)
    return response.json()



def sites_custom_list_parser():
    """
    Function to parse the response and create a customer list.

    Returns:
        list: List of customer dictionaries.

    """
    customer_list = []

    # Iterate over each item in the 'sites' list in the response
    for item in s1get_combined()['data']['sites']:
        id = item['id']
        name = item['name']
        sku = item['sku']
        activelicenses = item['activeLicenses']
        updatedat = item['updatedAt']

        # Create a customer dictionary with the extracted information
        customer = {
            'id': id,
            'name': name,
            'sku': sku,
            'activelicenses': activelicenses,
            'updatedat': updatedat
        }

        # Add the customer dictionary to the customer list
        customer_list.append(customer)
    return customer_list


# Function voor het schrijven van de CSV file
def write_csv_file():
    """
    Function to write data to a CSV file.

    """
    data_file = open('jsonoutput.csv', 'w', newline='')
    # Create a CSV writer object
    csv_writer = csv.writer(data_file)
    
    count = 0
    for data in sites_custom_list_parser():
        if count == 0:
            # Write the header row using the keys of the first data dictionary
            header = data.keys()
            csv_writer.writerow(header)
            count += 1

        # Write the data row using the values of the data dictionary
        csv_writer.writerow(data.values())
    # Close the CSV file
    data_file.close()


"""
Call function 'write_csv_file' to write data to a CSV file.
"""
write_csv_file()